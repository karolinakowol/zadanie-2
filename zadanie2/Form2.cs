﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace zadanie2
{
    public partial class Form2 : Form
    {
        Person person;
        public Form2(Person person)
        {
            InitializeComponent();
            this.person = person;

            string imie = person.Name;
            string nazwisko = person.Surname;
            string addres = person.Addres;

            label1.Text = "Twoje imie to: " + imie;
            label2.Text = "Twoje nazwisko to: " + nazwisko;
            label3.Text = "Twój adres to: " + addres;

        }
    }
}
